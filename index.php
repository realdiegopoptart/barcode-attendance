<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Attendance - team6479.org</title>

  <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css"
      type="text/css"
    />

  <script src="assets/library/jquery.min.js"></script>
  <script src="../dist/components/form.js"></script>
  <script src="../dist/components/transition.js"></script>

  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            barcode: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please scan your student id'
                },
                {
                  type   : 'length[6]',
                  prompt : 'Invalid student id'
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui yellow image header">
      <img src="team6479.png" class="ui massive image">
      <div class="content">
        Team 6479 Attendance<br><small>Scan your student id</small>
      </div>
    </h2>
    <form class="ui large form">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="barcode" placeholder="Barcode Scanner">
          </div>
        </div>
        <div class="ui fluid large blue submit button">Confirm</div>
      </div>

      <div class="ui error message"></div>

    </form>

    <div class="ui message">
      <a href="#">Officer control panel</a>
    </div>
  </div>
</div>

</body>

</html>
